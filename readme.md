### Todo application

Description:

- [x] A todo list screen and a detailed screen.
- [x] Support to Add/Edit/Complete/Delete a todo item.
- [x] Support to add an image in a todo item and display(save image in the local)
- [x] Support to rotate(Portrait/Landscape)

Requirements:

- [x] React-Native 0.61+
- [x] TypeScript
- [x] React-Navigation
- [x] useReducer
- [x] UserContext
- [x] AsyncStorage


### How to getting started

1. npm install
2. cd ios && pod install && cd ..
3. npm run ios

### Demos

![Preview Image](demo/preview.jpg)
[Demo 1](demo/1.png)
[Demo 2](demo/2.png)
[Demo 3](demo/3.png)
[Demo 4](demo/4.png)
[Demo 5](demo/5.png)
[Demo 6](demo/6.png)

### Issues

- Android modal backdrop

### Todos

1. Add localization
2. Support themes
3. Add more functionality likes:
    - sorting, filtering, grouping.
    - reminder, due date
    - ...
4. Release beta version
    - Logo
    - Splash

5. Testing.

---
### Reviewed on Apr 20/04/2020

**Previous Reviews:**
- [x] Need to remove the listener  in `componentWillUnmount` if add listener in `componentDidMount`
- [x] Do not use the absolute position style if it is not necessary.
    > There are only 2 element have absolute position and it is by intent. It is fab button ( at the bottom right corner), and the orther is todo input.
- [x] The keyboard should not cover the todo list. keep pushing todo list up when adding a todo.
    > Fix: auto scroll to new added item. FlatList scrollable and the scrollarea is not overlap with keyboard.

**Need to improve:**

- [x] Avoid using `any` as much as possible.

- [x] Avoid to abuse `Context`

- [x] Learn  how to `useRef`

- [x] Learn how to use `ListEmptyComponent` of FlatList to replace `EmptyTaskComponennt`.

- [x] Learn how to use ` react.memo` to avoid unnecessary render

- [x] Avoid load data every time when page rendering.