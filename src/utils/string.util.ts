export class StringUtils {
    public static IsNullOrEmpty(value: string): boolean {
        return value == null || value == undefined || value.trim().length == 0;
    }
}