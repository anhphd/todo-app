import 'react-native-gesture-handler';
import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import { HomePage } from './pages/home/home.page';
import { DetailPage } from './pages/detail/detail.page';
import { AppProvider } from './providers/app.provider';
import { TodoProvider } from './providers/todo.provider';

const AppStack = createStackNavigator();

const App = () => {
  return (<AppProvider>
    <TodoProvider>
      <NavigationContainer>
        <AppStack.Navigator>
          <AppStack.Screen name="Todos" component={HomePage} options={{ headerShown: false }}></AppStack.Screen>
          <AppStack.Screen name="Details" component={DetailPage} options={{ headerShown: false }}></AppStack.Screen>
        </AppStack.Navigator>
      </NavigationContainer>
    </TodoProvider>
  </AppProvider>);
}

export default App;