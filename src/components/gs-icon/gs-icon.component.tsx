import React from 'react';

import IconAdd from './icons/icon-add.svg';
import IconAttachment from './icons/icon-attachment.svg';
import IconBack from './icons/icon-back.svg';
import IconBookmark from './icons/icon-bookmark.svg';
import IconBookmarked from './icons/icon-bookmarked.svg';
import IconCheckBoxChecked from './icons/icon-checkbox-checked.svg';
import IconCheckBox from './icons/icon-checkbox.svg';
import IconClose from './icons/icon-close.svg';
import IconCompleted from './icons/icon-completed.svg';
import IconDate from './icons/icon-date.svg';
import IconDelete from './icons/icon-delete.svg';
import IconEdit from './icons/icon-edit.svg';
import IconImages from './icons/icon-images.svg';
import IconMore from './icons/icon-more.svg';
import IconNote from './icons/icon-note.svg';
import IconReminder from './icons/icon-reminder.svg';
import IconRepeat from './icons/icon-repeat.svg';
import IconSortList from './icons/icon-sort-list.svg';
import IconSort from './icons/icon-sort.svg';
import IconSubmitEnabled from './icons/icon-submit-enabled.svg';
import IconSubmit from './icons/icon-submit.svg';
import IconTheme from './icons/icon-theme.svg';
import IconUp from './icons/icon-up.svg';

import { CurrentTheme as Theme } from '../../config/theme.config';


interface IconProps {
    name: string;
    width?: number | string;
    height?: number | string;
    color?: string;
    size?: 'default' | 'small' | 'medium' | 'large' | 'xlarge' | 'xxlarge' | 'xxxlarge'
}

export const Icons = {
    Add: 'Add',
    Attachment: 'Attachment',
    Back: 'Back',
    Bookmark: 'Bookmark',
    Bookmarked: 'Bookmarked',
    CheckBoxChecked: 'CheckBoxChecked',
    CheckBox: 'CheckBox',
    Close: 'Close',
    Completed: 'Completed',
    Date: 'Date',
    Delete: 'Delete',
    Edit: 'Edit',
    Images: 'Images',
    More: 'More',
    Note: 'Note',
    Reminder: 'Reminder',
    Repeat: 'Repeat',
    SortList: 'SortList',
    Sort: 'Sort',
    SubmitEnabled: 'SubmitEnabled',
    Submit: 'Submit',
    Theme: 'Theme',
    Up: 'Up'
}

export const GsIcon = (props: IconProps) => {
    let width = props.width == undefined ? Theme.buttonSizes.medium : props.width;
    let height = props.height == undefined ? Theme.buttonSizes.medium : props.height;
    let color = props.color ? props.color : undefined;
    if (props.size != undefined) {
        switch (props.size) {
            case 'default': width = Theme.buttonSizes.medium; height = Theme.buttonSizes.medium; break;
            case 'small': width = Theme.buttonSizes.small; height = Theme.buttonSizes.small; break;
            case 'medium': width = Theme.buttonSizes.medium; height = Theme.buttonSizes.medium; break;
            case 'large': width = Theme.buttonSizes.large; height = Theme.buttonSizes.large; break;
            case 'xlarge': width = Theme.buttonSizes.xlarge; height = Theme.buttonSizes.xlarge; break;
            case 'xxlarge': width = Theme.buttonSizes.xxlarge; height = Theme.buttonSizes.xxlarge; break;
            case 'xxxlarge': width = Theme.buttonSizes.xxxlarge; height = Theme.buttonSizes.xxxlarge; break;
            default: width = Theme.buttonSizes.medium; height = Theme.buttonSizes.medium; break;
        }
    }
    switch (props.name) {
        case Icons.Add:
            return <IconAdd width={width} height={height} fill={color} />
        case Icons.Attachment:
            return <IconAttachment width={width} height={height} fill={color} />
        case Icons.Back:
            return <IconBack width={width} height={height} fill={color} />
        case Icons.Bookmark:
            return <IconBookmark width={width} height={height} fill={color} />
        case Icons.Bookmarked:
            return <IconBookmarked width={width} height={height} fill={color} />
        case Icons.CheckBox:
            return <IconCheckBox width={width} height={height} fill={color} />
        case Icons.CheckBoxChecked:
            return <IconCheckBoxChecked width={width} height={height} fill={color} />
        case Icons.Close:
            return <IconClose width={width} height={height} fill={color} />
        case Icons.Completed:
            return <IconCompleted width={width} height={height} fill={color} />
        case Icons.Date:
            return <IconDate width={width} height={height} fill={color} />
        case Icons.Delete:
            return <IconDelete width={width} height={height} fill={color} />
        case Icons.Edit:
            return <IconEdit width={width} height={height} fill={color} />
        case Icons.Images:
            return <IconImages width={width} height={height} fill={color} />
        case Icons.Note:
            return <IconNote width={width} height={height} fill={color} />
        case Icons.More:
            return <IconMore width={width} height={height} fill={color} />
        case Icons.Reminder:
            return <IconReminder width={width} height={height} fill={color} />
        case Icons.Repeat:
            return <IconRepeat width={width} height={height} fill={color} />
        case Icons.SortList:
            return <IconSortList width={width} height={height} fill={color} />
        case Icons.Sort:
            return <IconSort width={width} height={height} fill={color} />
        case Icons.SubmitEnabled:
            return <IconSubmitEnabled width={width} height={height} fill={color} />
        case Icons.Submit:
            return <IconSubmit width={width} height={height} fill={color} />
        case Icons.Theme:
            return <IconTheme width={width} height={height} fill={color} />
        case Icons.Up:
            return <IconUp width={width} height={height} fill={color} />
    }
    return <IconAttachment></IconAttachment>
}
