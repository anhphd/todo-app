import React, { useState, useContext, useEffect } from "react";
import { View, Text, Alert, Animated } from "react-native";
import { ScrollView, TouchableWithoutFeedback, TouchableNativeFeedback } from "react-native-gesture-handler";
import { SortBy, TodoContext } from "../../context/todo.context";
import { GsButton } from "../gs-button.component";
import { GsIcon, Icons } from "../gs-icon/gs-icon.component";
import * as GsStyles from '../../styles/index';

type Props = {
    dismiss?: () => void;
}
enum ViewType {
    NONE, SORT
}

const onClickImplementingFeature = () => {
    Alert.alert(
        'Oops!',
        `This feature hasn't available yet`,
        [
            {
                text: 'OK', onPress: () => {
                }
            }
        ],
        { cancelable: false }
    )
};

export const SettingModal = (props: Props) => {

    const { updateSettings, settings, removeAllItems } = useContext(TodoContext);

    const [state, setState] = useState({ viewType: ViewType.NONE });

    return <View style={[GsStyles.Flex.Fill]}>
        <GsButton style={GsStyles.Flex.Fill} onPress={props.dismiss}></GsButton>
        <View style={{ backgroundColor: 'white', paddingBottom: 16, borderTopLeftRadius: 16, borderTopRightRadius: 16 }}>
            <View style={[GsStyles.Flex.Row, GsStyles.Flex.AlignItemsCenter, GsStyles.Borders.BorderBottom]}>
                {
                    state.viewType == ViewType.SORT && <GsButton onPress={() => { setState({ viewType: ViewType.NONE }); }}>
                        <GsIcon name={Icons.Back} color='black' size='medium'></GsIcon>
                        <Text>Back</Text>
                    </GsButton>
                }
                <Text style={{ flex: 1, padding: 16, textAlign: "center", fontSize: 18, fontWeight: "bold" }}>{state.viewType == ViewType.NONE ? "List Options" : "Sort By"}</Text>
                <GsButton onPress={() => { if (props.dismiss) props.dismiss(); }} style={GsStyles.Spacing.PaddingVertical}>
                    <Text style={[GsStyles.Typography.Subtitle1, GsStyles.Colors.Secondary]}>Done</Text>
                </GsButton>
            </View>

            <ScrollView style={GsStyles.SpacingSmall.PaddingVertical}>
                {state.viewType == ViewType.NONE && <View>

                    <GsButton onPress={onClickImplementingFeature}>
                        <GsIcon name={Icons.Edit} size='medium' color="black"></GsIcon>
                        <Text style={GsStyles.Spacing.PaddingHorizontal}>Edit</Text>
                    </GsButton>

                    <GsButton onPress={() => { setState({ viewType: ViewType.SORT }) }}>
                        <GsIcon name={Icons.SortList} size='medium' color="black"></GsIcon>
                        <Text style={GsStyles.Spacing.PaddingHorizontal}>Sort</Text>
                    </GsButton>


                    <GsButton onPress={onClickImplementingFeature}>
                        <GsIcon name={Icons.Theme} size='medium' color="black"></GsIcon>
                        <Text style={GsStyles.Spacing.PaddingHorizontal} >Change Theme</Text>
                    </GsButton>

                    <GsButton onPress={() => { updateSettings({ ...settings, hideCompletedTask: !settings.hideCompletedTask }); if (props.dismiss) props.dismiss(); }}>
                        <GsIcon name={Icons.Completed} size='medium' color="black"></GsIcon>
                        <Text style={GsStyles.Spacing.PaddingHorizontal}>{settings.hideCompletedTask ? "Show Completed Tasks" : "Hide Completed Tasks"}</Text>
                    </GsButton>

                    <GsButton onPress={() => { removeAllItems(); if (props.dismiss) props.dismiss(); }}>
                        <GsIcon name={Icons.Delete} size='medium' color="black"></GsIcon>
                        <Text style={GsStyles.Spacing.PaddingHorizontal}>Remove All Tasks</Text>
                    </GsButton>

                </View>
                }
                {
                    state.viewType == ViewType.SORT && <View>
                        <GsButton onPress={() => { updateSettings({ ...settings, sortBy: SortBy.MODIFIED_AT }); if (props.dismiss) props.dismiss(); }}>
                            <Text>Modify Time</Text>
                        </GsButton>
                        <GsButton onPress={() => { updateSettings({ ...settings, sortBy: SortBy.CREATED_AT }); if (props.dismiss) props.dismiss(); }}>
                            <Text>Creation Date</Text>
                        </GsButton>
                        <GsButton onPress={() => { updateSettings({ ...settings, sortBy: SortBy.NAME }); if (props.dismiss) props.dismiss(); }}>
                            <Text>Name</Text>
                        </GsButton>
                        <GsButton onPress={() => { updateSettings({ ...settings, sortBy: SortBy.PRIORITY }); if (props.dismiss) props.dismiss(); }}>
                            <Text>Priority</Text>
                        </GsButton>
                        <GsButton onPress={() => { updateSettings({ ...settings, sortBy: SortBy.NONE }); if (props.dismiss) props.dismiss(); }}>
                            <Text>None</Text>
                        </GsButton>
                    </View>
                }

            </ScrollView>
        </View>
    </View>
}