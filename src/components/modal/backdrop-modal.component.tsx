import React from "react";
import { Animated } from "react-native";

type Props = {
    children: any
}

export const BackDropModal = (props: Props) => {

    let opacity = new Animated.Value(0);

    Animated.timing(opacity, {
        toValue: 1,
        duration: 250,
        useNativeDriver: true
    }).start();

    return <Animated.View style={{ height: "100%", backgroundColor: 'rgba(0,0,0,0.4)', opacity: opacity }}>
        {props.children}
    </Animated.View>

}

