import React, { useState } from "react";
import { View, Text, TextInput } from "react-native";
import { GsButton } from "../gs-button.component";
import { GsIcon, Icons } from "../gs-icon/gs-icon.component";
import * as GsStyles from '../../styles/index';
import { CurrentTheme as Theme } from '../../config/theme.config';
type Props = {
    value: string,
    title: string,
    onDissmiss: (value: string) => void,
    onChange?: (value: string) => void,
    placeholder?: string
}

export const InputFormModal = ({ title, value, onChange, onDissmiss, placeholder }: Props) => {

    const [state, setState] = useState({ value: value });

    return <View style={[GsStyles.Flex.Fill, GsStyles.Colors.BgLight, GsStyles.Spacing.PaddingVertical]}>
        <View style={[GsStyles.Flex.Row, GsStyles.Flex.AlignItemsCenter]}>
            <GsButton onPress={() => { onDissmiss(state.value) }}>
                <GsIcon name={Icons.Back} color={Theme.colors.dark} size='medium'></GsIcon>
            </GsButton>
            <Text style={[GsStyles.Typography.H6, GsStyles.Flex.Fill]}>{title}</Text>
        </View>
        <View style={GsStyles.Spacing.Padding}>
            <TextInput autoFocus={true} placeholder={placeholder} value={state.value} onChangeText={(text) => {
                if (onChange) onChange(text);
                setState({ value: text });
            }} />
        </View>
    </View>;
}