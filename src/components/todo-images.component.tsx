import React, { useState } from 'react';
import { View, Text, Image } from 'react-native';
import { GsButton } from './gs-button.component';
import { GsIcon, Icons } from './gs-icon/gs-icon.component';
import ImagePicker from 'react-native-image-picker';
import * as GsStyles from '../styles/index';

export const TodoImagesComponent = ({ images, onChange }: { images: Array<string>, onChange: (images: Array<string>) => void }) => {

    const [Images, setImages] = useState(images);

    const onClickAddImage = () => {
        const options = {
            title: 'Add An Image',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };

        ImagePicker.showImagePicker(options, (response) => {
            if (response.didCancel) {
                // todo
            } else if (response.error) {
                // todo
            } else {
                setImages([response.uri, ...Images]);
                onChange(Images)
            }
        });
    }


    return <View>
        <View style={[GsStyles.Colors.BgLight, GsStyles.Borders.BorderRadiusSmall, GsStyles.Spacing.MarginTop, GsStyles.SpacingSmall.MarginHorizontal]}>
            <GsButton style={GsStyles.Spacing.NoPaddingVertical} onPress={onClickAddImage}>
                <GsIcon name={Icons.Images} size="large"></GsIcon>
                <Text style={[GsStyles.Flex.Fill, GsStyles.Spacing.PaddingVertical, GsStyles.Spacing.MarginLeft]}>Add Image</Text>
            </GsButton>
        </View>
        {<View style={[GsStyles.SpacingSmall.Padding, GsStyles.Flex.Row, GsStyles.Flex.Wrap]} >
            {
                Images.map((url, index) => {
                    return <Image resizeMode='cover' key={index} source={{ uri: url }} style={{ width: 100, height: 100, marginRight: 5, marginBottom: 5 }}></Image>
                })
            }
        </View>
        }
    </View>
}