import React, { useContext } from 'react';
import { TodoContext } from '../context/todo.context';
import { FlatList, View, Text } from 'react-native';
import { TodoItemComponent } from './todo-item.component';

import * as GsStyles from '../styles/index';
import { ITodoItem } from '../models/todo.model';

const ListEmptyComponent = () => {
    return <View style={GsStyles.Flex.AlignItemsCenter}>
        <Text style={GsStyles.Spacing.PaddingVertical}>You have no task, create new?</Text>
        <Text>Or enjoy your day off</Text>
    </View>
}

type Props = {
}

export const TodoListComponent = React.memo(React.forwardRef<FlatList<ITodoItem>, Props>((props: Props, ref) => {

    const { todos, settings } = useContext(TodoContext);

    const items = todos.filter(ele => {
        return !ele.IsCompleted || !settings.hideCompletedTask;
    });

    return <FlatList
        ref={ref}
        data={items}
        renderItem={({ item }) => <TodoItemComponent Item={item} key={item.ItemId} />}
        keyExtractor={item => item.ItemId}
        onScrollToIndexFailed={({ index, highestMeasuredFrameIndex, averageItemLength }) => { }}
        ListEmptyComponent={() => <ListEmptyComponent />}
    />
}));
