import React, { useState, useEffect } from 'react';
import { View, Animated } from 'react-native';
import { GsIcon, Icons } from './gs-icon/gs-icon.component';
import { GsButton } from './gs-button.component';

type FabButtonProps = {
    onPress: () => void,
    visible?: boolean
}

export const FabButton = ({ visible, onPress }: FabButtonProps) => {
    return !visible ? null : <View style={{ position: 'absolute', right: 16, bottom: 32 }}>
        <GsButton onPress={onPress}>
            <GsIcon name={Icons.Add} size="xxxlarge"></GsIcon>
        </GsButton>
    </View>
}
