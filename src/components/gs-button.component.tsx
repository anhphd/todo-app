import React from 'react';

import { TouchableWithoutFeedback, View, ViewStyle } from "react-native";
import * as GsStyles from '../styles/index';
type Props = {
    onPress?: () => void;
    children?: any,
    style?: ViewStyle | ViewStyle[]
}

export const GsButton = (props: Props) => {
    return <TouchableWithoutFeedback onPress={props.onPress}>
        <View style={[GsStyles.Flex.Row, GsStyles.Flex.AlignItemsCenter, GsStyles.Spacing.PaddingHorizontal, GsStyles.SpacingSmall.PaddingVertical, props.style]} >
            {props.children}
        </View>
    </TouchableWithoutFeedback>
}