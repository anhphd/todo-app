import React from 'react';
import { GsIcon, Icons } from './gs-icon/gs-icon.component';
import { GsButton } from './gs-button.component';
import * as GsStyles from '../styles/index';

type CheckBoxProps = {
    value?: boolean,
    onPress?: () => void
}

export const BookMarkComponent = ({ value, onPress }: CheckBoxProps) => {
    return <GsButton onPress={onPress} style={[GsStyles.Flex.Row, GsStyles.Flex.AlignItemsCenter, GsStyles.Spacing.PaddingHorizontal]}>
        <GsIcon name={value ? Icons.Bookmarked : Icons.Bookmark} size="large"></GsIcon>
    </GsButton>
}