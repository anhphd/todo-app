import React, { useContext, useState, useEffect } from 'react';
import { TodoContext, SortBy, SortType } from '../context/todo.context';
import { View, Text, Animated } from 'react-native';
import { GsIcon, Icons } from './gs-icon/gs-icon.component';
import { GsButton } from './gs-button.component';
import * as GsStyles from '../styles/index';
import { CurrentTheme as Theme } from '../config/theme.config';

export const TodoListFilterComponent = () => {

    const { settings, updateSettings, todos } = useContext(TodoContext);

    const [state, setState] = useState({
        animate: new Animated.Value(0),
        visible: settings.sortBy != SortBy.NONE
    });

    const interpolationRotation = state.animate.interpolate({
        inputRange: [0, 1],
        outputRange: ['0deg', '180deg']
    });

    const animateStyle = {
        transform: [
            { rotate: interpolationRotation }
        ]
    }

    const toggleSortType = () => {
        updateSettings({...settings,sortType : (settings.sortType == SortType.INCREASE) ? SortType.DECREASE : SortType.INCREASE});
        Animated.timing(state.animate, {
            toValue: settings.sortType == SortType.INCREASE ? 0 : 1,
            duration: 100,
            useNativeDriver: true
        }).start();
    }

    useEffect(() => {
        setState({ ...state, visible: todos.length != 0 && settings.sortBy != SortBy.NONE });
    }, [settings, todos]);

    return !state.visible ? null : <View style={[GsStyles.Flex.Row]}>
        <GsButton onPress={toggleSortType}>
            <View style={{ flexDirection: 'row', alignItems: 'center', alignSelf: 'flex-start', padding: 10, borderRadius: 4, backgroundColor: "rgba(0,0,0,0.6)" }}>
                <Text style={GsStyles.Colors.Light}>Sorted by {settings.sortBy}</Text>
                <Animated.View style={[GsStyles.SpacingSmall.PaddingHorizontal, animateStyle]}><GsIcon name={Icons.Up} size='small'></GsIcon></Animated.View>
            </View>
        </GsButton>
        <GsButton onPress={() => {
            updateSettings({ ...settings, sortBy: SortBy.NONE });
        }} style={GsStyles.Spacing.NoPadding}>
            <GsIcon name={Icons.Close} size='small' color={Theme.colors.gray}></GsIcon>
        </GsButton>
    </View>
}
