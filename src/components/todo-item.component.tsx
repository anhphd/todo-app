import React, { useContext } from 'react';
import { View, Text, TouchableWithoutFeedback, StyleSheet } from 'react-native';
import { ITodoItem } from '../models/todo.model';
import { CheckBoxComponent } from './checkbox.component';
import { TodoContext } from '../context/todo.context';
import { useNavigation } from '@react-navigation/native';
import { BookMarkComponent } from './bookmark.component';
import { StringUtils } from '../utils/string.util';
import { GsIcon, Icons } from './gs-icon/gs-icon.component';

import * as GsStyles from '../styles/index';

type TodoItemProps = {
    Item: ITodoItem,
    Editable?: boolean
};

const styles = StyleSheet.create({
    container: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 10,
        backgroundColor: 'white',
        marginHorizontal: 16,
        borderRadius: 6,
    }
})

export const TodoItemComponent = ({ Item }: TodoItemProps) => {
    const { toggleCompleted, updateItem } = useContext(TodoContext);
    const navigation = useNavigation();
    const onCheckBoxPressed = () => { toggleCompleted(Item.ItemId); };
    const onBookMarkPressed = () => { updateItem({ ...Item, IsBookMarked: !Item.IsBookMarked }); };
    const onClickText = () => { navigation.navigate("Details", { ItemId: Item.ItemId }); };

    const hasImageAttached = Item.Images.length > 0;
    const hasNote = !StringUtils.IsNullOrEmpty(Item.Note);

    return <View style={styles.container}>
        <CheckBoxComponent value={Item.IsCompleted} onPress={onCheckBoxPressed}></CheckBoxComponent>

        <TouchableWithoutFeedback onPress={onClickText}>
            <View style={[GsStyles.Flex.Fill, GsStyles.Flex.JustifyContentCenter, GsStyles.Spacing.PaddingVertical]}>
                <Text style={Item.IsCompleted ? [GsStyles.Typography.Body1, GsStyles.Text.DecorationLineThrough] : [GsStyles.Typography.Body1]}>{Item.Title}</Text>
                <View style={GsStyles.Flex.Row}>
                    {
                        hasImageAttached && <View style={[GsStyles.Flex.Row, GsStyles.Flex.AlignItemsCenter, GsStyles.SpacingSmall.PaddingTop]}>
                            <GsIcon color="gray" name={Icons.Attachment} size="small"></GsIcon>
                            <Text style={[{ paddingLeft: 6, paddingRight: 12, color: 'gray' }, GsStyles.Typography.Subtitle2]}>Files Attached</Text>
                        </View>
                    }
                    {
                        hasNote && <View style={[GsStyles.Flex.Row, GsStyles.Flex.AlignItemsCenter, GsStyles.SpacingSmall.PaddingTop]}>
                            <GsIcon color="gray" name={Icons.Note} size="small"></GsIcon>
                            <Text style={[{ paddingLeft: 6, paddingRight: 12, color: 'gray', fontSize: 12 }, GsStyles.Typography.Subtitle2]}>Note</Text>
                        </View>
                    }
                </View>
            </View>
        </TouchableWithoutFeedback>

        <BookMarkComponent value={Item.IsBookMarked} onPress={onBookMarkPressed}></BookMarkComponent>
    </View>
}
