import { StyleSheet } from "react-native";

import { CurrentTheme as Theme } from '../config/theme.config';

const UnitBase = Theme.fontSizes.base;
const UnitSmall = 0.625 * UnitBase;
const UnitLarge = 1.25 * UnitBase;

export const GsSpacing = StyleSheet.create({
    NoPadding: { paddingLeft: 0, paddingRight: 0, paddingTop: 0, paddingBottom: 0 },
    NoPaddingVertical: { paddingVertical: 0 },
    NoPaddingHorizontal: { paddingHorizontal: 0 },

    Padding: { paddingLeft: UnitBase, paddingRight: UnitBase, paddingTop: UnitBase, paddingBottom: UnitBase },
    PaddingTop: { paddingTop: UnitBase },
    PaddingBottom: { paddingBottom: UnitBase },
    PaddingLeft: { paddingLeft: UnitBase },
    PaddingRight: { paddingRight: UnitBase },
    PaddingVertical: { paddingVertical: UnitBase },
    PaddingHorizontal: { paddingHorizontal: UnitBase },

    NoMargin: { marginLeft: 0, marginRight: 0, marginTop: 0, marginBottom: 0 },
    NoMarginVertical: { marginVertical: 0 },
    NoMarginHorizontal: { marginHorizontal: 0 },

    Margin: { marginLeft: UnitBase, marginRight: UnitBase, marginTop: UnitBase, marginBottom: UnitBase },
    MarginTop: { marginTop: UnitBase },
    MarginBottom: { marginBottom: UnitBase },
    MarginLeft: { marginLeft: UnitBase },
    MarginRight: { marginRight: UnitBase },
    MarginVertical: { marginVertical: UnitBase },
    MarginHorizontal: { marginHorizontal: UnitBase },

});

export const GsSpacingSmall = StyleSheet.create({
    NoPadding: { paddingLeft: 0, paddingRight: 0, paddingTop: 0, paddingBottom: 0 },
    NoPaddingVertical: { paddingVertical: 0 },
    NoPaddingHorizontal: { paddingHorizontal: 0 },

    Padding: { paddingLeft: UnitSmall, paddingRight: UnitSmall, paddingTop: UnitSmall, paddingBottom: UnitSmall },
    PaddingTop: { paddingTop: UnitSmall },
    PaddingBottom: { paddingBottom: UnitSmall },
    PaddingLeft: { paddingLeft: UnitSmall },
    PaddingRight: { paddingRight: UnitSmall },
    PaddingVertical: { paddingVertical: UnitSmall },
    PaddingHorizontal: { paddingHorizontal: UnitSmall },

    NoMargin: { marginLeft: 0, marginRight: 0, marginTop: 0, marginBottom: 0 },
    NoMarginVertical: { marginTop: 0, marginBottom: 0 },
    NoMarginHorizontal: { marginLeft: 0, marginRight: 0 },

    Margin: { marginLeft: UnitSmall, marginRight: UnitSmall, marginTop: UnitSmall, marginBottom: UnitSmall },
    MarginTop: { marginTop: UnitSmall },
    MarginBottom: { marginBottom: UnitSmall },
    MarginLeft: { marginLeft: UnitSmall },
    MarginRight: { marginRight: UnitSmall },
    MarginVertical: { marginVertical: UnitSmall },
    MarginHorizontal: { marginHorizontal: UnitSmall },

});

export const GsSpacingLarge = StyleSheet.create({
    NoPadding: { paddingLeft: 0, paddingRight: 0, paddingTop: 0, paddingBottom: 0 },
    NoPaddingVertical: { paddingVertical: 0 },
    NoPaddingHorizontal: { paddingHorizontal: 0 },

    Padding: { paddingLeft: UnitLarge, paddingRight: UnitLarge, paddingTop: UnitLarge, paddingBottom: UnitLarge },
    PaddingTop: { paddingTop: UnitLarge },
    PaddingBottom: { paddingBottom: UnitLarge },
    PaddingLeft: { paddingLeft: UnitLarge },
    PaddingRight: { paddingRight: UnitLarge },
    PaddingVertical: { paddingVertical: UnitLarge },
    PaddingHorizontal: { paddingHorizontal: UnitLarge },

    NoMargin: { marginLeft: 0, marginRight: 0, marginTop: 0, marginBottom: 0 },
    NoMarginVertical: { marginTop: 0, marginBottom: 0 },
    NoMarginHorizontal: { marginLeft: 0, marginRight: 0 },

    Margin: { marginLeft: UnitLarge, marginRight: UnitLarge, marginTop: UnitLarge, marginBottom: UnitLarge },
    MarginTop: { marginTop: UnitLarge },
    MarginBottom: { marginBottom: UnitLarge },
    MarginLeft: { marginLeft: UnitLarge },
    MarginRight: { marginRight: UnitLarge },
    MarginVertical: { marginVertical: UnitLarge },
    MarginHorizontal: { marginHorizontal: UnitLarge },

});