import { StyleSheet } from "react-native";

import { CurrentTheme as Theme } from '../config/theme.config';

export const GsColors = StyleSheet.create({
    Primary: { color: Theme.colors.primary },
    Secondary: { color: Theme.colors.secondary },
    Danger: { color: Theme.colors.danger },
    Warning: { color: Theme.colors.warning },
    Gray: { color: Theme.colors.gray },
    Light: { color: Theme.colors.light },
    Dark: { color: Theme.colors.dark },
    Background: { color: Theme.colors.background },
    BgPrimary: { backgroundColor: Theme.colors.primary },
    BgSecondary: { backgroundColor: Theme.colors.secondary },
    BgDanger: { backgroundColor: Theme.colors.danger },
    BgWarning: { backgroundColor: Theme.colors.warning },
    BgGray: { backgroundColor: Theme.colors.gray },
    BgLight: { backgroundColor: Theme.colors.light },
    BgDark: { backgroundColor: Theme.colors.dark },
    BgBackground: { backgroundColor: Theme.colors.background }
});
