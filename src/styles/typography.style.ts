import { StyleSheet } from "react-native";

import { CurrentTheme as Theme } from '../config/theme.config';

enum FontWeights {
    Thin = '100',
    ExtraLight = '200',
    Light = '300',
    Regular = '400',
    Medium = '500',
    SemiBold = '600',
    Bold = '700',
    ExtraBold = '800',
    Black = '900',
    ExtraBlack = '950'
}

export const GsTypography = StyleSheet.create({
    H1: { fontSize: Theme.fontSizes.h1, fontWeight: FontWeights.Light, letterSpacing: -1.5 },
    H2: { fontSize: Theme.fontSizes.h2, fontWeight: FontWeights.Light, letterSpacing: -0.5 },
    H3: { fontSize: Theme.fontSizes.h3, fontWeight: FontWeights.Regular, letterSpacing: 0 },
    H4: { fontSize: Theme.fontSizes.h4, fontWeight: FontWeights.Regular, letterSpacing: 0.25 },
    H5: { fontSize: Theme.fontSizes.h5, fontWeight: FontWeights.Regular, letterSpacing: 0 },
    H6: { fontSize: Theme.fontSizes.h6, fontWeight: FontWeights.Medium, letterSpacing: 0.15 },
    Subtitle1: { fontSize: Theme.fontSizes.subtitle1, fontWeight: FontWeights.Regular, letterSpacing: 0.15 },
    Subtitle2: { fontSize: Theme.fontSizes.subtitle2, fontWeight: FontWeights.Medium, letterSpacing: 0.1 },
    Body1: { fontSize: Theme.fontSizes.body1, fontWeight: FontWeights.Regular, letterSpacing: 0.5 },
    Body2: { fontSize: Theme.fontSizes.body2, fontWeight: FontWeights.Regular, letterSpacing: 0.25 },
    Button: { fontSize: Theme.fontSizes.button, fontWeight: FontWeights.Medium, letterSpacing: 1.25 },
    Caption: { fontSize: Theme.fontSizes.caption, fontWeight: FontWeights.Regular, letterSpacing: 0.4 },
    Overline: { fontSize: Theme.fontSizes.overline, fontWeight: FontWeights.Regular, letterSpacing: 1.5 },
});