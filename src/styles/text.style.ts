import { StyleSheet } from "react-native";

export const GsText = StyleSheet.create({
    AlignCenter: { textAlign: "center" },
    AlignJustify: { textAlign: "justify" },
    AlignLeft: { textAlign: "left" },
    AlignRight: { textAlign: "right" },

    TransformUpperCase: { textTransform: "uppercase" },
    TransformLowerCase: { textTransform: "lowercase" },
    TransformCapitalize: { textTransform: "capitalize" },

    DecorationLineThrough: { textDecorationLine: 'line-through' },
    DecorationUnderLine: { textDecorationLine: 'underline' },
    DecorationUnderLineAndLineThrough: { textDecorationLine: 'underline line-through' }
});