import { StyleSheet } from "react-native";

export const GsBorders = StyleSheet.create({
    BorderRadiusSmall: { borderRadius: 4 },
    BorderRadiusMedium: { borderRadius: 10 },
    BorderRadiusLarge: { borderRadius: 16 },

    BorderTopLeftRadius: { borderTopLeftRadius: 16 },
    BorderTopRightRadius: { borderTopLeftRadius: 16 },

    BorderBottom: {
        borderBottomColor: '#F0F0F0', borderBottomWidth: 1
    }
    // todo : add remaining styles...

});