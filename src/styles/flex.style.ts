import { StyleSheet } from "react-native";

export const GsFlex = StyleSheet.create({
    AlignSelfStart: { alignSelf: 'flex-start' },
    AlignSelfEnd: { alignSelf: 'flex-end' },
    AlignSelfCenter: { alignSelf: 'center' },
    AlignSelfStretch: { alignSelf: 'stretch' },
    AlignSelfBaseLine: { alignSelf: 'baseline' },
    AlignSelfAuto: { alignSelf: 'auto' },

    JustifyContentStart: { justifyContent: "flex-start" },
    JustifyContentEnd: { justifyContent: "flex-end" },
    JustifyContentCenter: { justifyContent: "center" },
    JustifyContentSpaceAround: { justifyContent: "space-around" },
    JustifyContentSpaceBetween: { justifyContent: "space-between" },
    JustifyContentSpaceEvenly: { justifyContent: "space-evenly" },

    AlignItemsStart: { alignItems: 'flex-start' },
    AlignItemsEnd: { alignItems: 'flex-end' },
    AlignItemsCenter: { alignItems: 'center' },
    AlignItemsStretch: { alignItems: 'stretch' },
    AlignItemsBaseLine: { alignItems: 'baseline' },

    Wrap: { flexWrap: 'wrap' },
    NoWrap: { flexWrap: 'nowrap' },
    WrapReverse: { flexWrap: 'wrap-reverse' },

    Row: { flexDirection: 'row' },
    RowReverse: { flexDirection: 'row-reverse' },
    Column: { flexDirection: 'column' },
    ColumnReverse: { flexDirection: 'column-reverse' },

    Fill: { flex: 1 },
});