import { GsTypography as Typography } from './typography.style';
import { GsFlex as Flex } from './flex.style';
import { GsSpacing as Spacing, GsSpacingSmall as SpacingSmall, GsSpacingLarge as SpacingLarge } from './spacing.style';
import { GsText as Text } from './text.style';
import { GsColors as Colors } from './color.style';
import { GsBorders as Borders } from './border.style';

export { Typography, Flex, Spacing, SpacingSmall, SpacingLarge, Text, Colors, Borders };