import React, { useEffect, useContext, useState, useLayoutEffect } from 'react';
import { View, ScrollView, Text, Alert, Image, KeyboardAvoidingView, StatusBar, Platform, TextInput, Button, Modal, } from "react-native";
import { TodoContext } from '../../context/todo.context';
import { GsIcon, Icons } from '../../components/gs-icon/gs-icon.component';
import { RouteProp } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import DetailPageStyles from './detail.style';
import ImagePicker from 'react-native-image-picker';
import { CheckBoxComponent } from '../../components/checkbox.component';
import { BookMarkComponent } from '../../components/bookmark.component';
import { SafeAreaView } from 'react-native-safe-area-context';
import { GsButton } from '../../components/gs-button.component';

import * as GsStyles from '../../styles/index';
import { CurrentTheme as Theme } from '../../config/theme.config';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import { InputFormModal } from '../../components/modal/inputform-modal.component';
import { TodoImagesComponent } from '../../components/todo-images.component';
import { TodoItem } from '../../models/todo.model';

type RouterParams = {
    Item: {
        ItemId: string
    }
}

type DetailPageRouteProps = RouteProp<RouterParams, 'Item'>;

type DetailPageNavigationProp = StackNavigationProp<RouterParams, 'Item'>

type Props = {
    route: DetailPageRouteProps,
    navigation: DetailPageNavigationProp
}

const styles = DetailPageStyles;

enum ModifyStatus {
    NONE = 0,
    MODIFIED = 1,
    SAVED = 2
}

export const DetailPage = ({ navigation, route }: Props) => {

    const { getItem, updateItem, removeItem } = useContext(TodoContext);

    const [state, setState] = useState({
        ItemId: route.params.ItemId,
        Item: new TodoItem(),
        Status: ModifyStatus.NONE,
        IsEditingNote: false
    });



    useLayoutEffect(() => {
        setState({
            ...state,
            Item: getItem(route.params.ItemId),
        });
    }, []);

    const onClickImplementingFeature = () => {
        Alert.alert(
            'Oops!',
            'This feature hasn\'t available yet',
            [{ text: 'OK', onPress: () => { } }],
            { cancelable: false }
        )
    }

    if (Platform.OS == 'android') {
        useEffect(() => {
            navigation.addListener('focus', () => {
                StatusBar.setBarStyle('dark-content');
                StatusBar.setBackgroundColor(Theme.colors.light);
            });

            return () => {
                navigation.removeListener('focus', () => { });
            }
        }, []);
    }

    const onClickBack = () => {
        if (state.Status === ModifyStatus.MODIFIED) {
            Alert.alert(
                'You have unsaved changes',
                'Do you want to save theme before leave?',
                [
                    { text: 'Save changes', onPress: () => { updateItem(state.Item); navigation.goBack(); } },
                    { text: 'Discard', onPress: () => { navigation.goBack(); } },
                    { text: 'Cancel', onPress: () => { }, style: 'cancel' },

                ],
                { cancelable: false }
            )
            return;
        }
        navigation.goBack();
    }

    const onClickDeleteItem = () => {
        Alert.alert(
            'Are you sure?',
            `${state.Item?.Title} will be permanatly deleted`,
            [
                { text: 'Cancel', onPress: () => { }, style: 'cancel' },
                {
                    text: 'OK', onPress: () => {
                        removeItem(state.ItemId);
                        navigation.pop();
                    }
                },
            ],
            { cancelable: false }
        )
    }
    return <KeyboardAvoidingView style={GsStyles.Flex.Fill} behavior={Platform.OS == 'ios' ? 'padding' : undefined}>
        <SafeAreaView style={{ flex: 1, backgroundColor: Theme.colors.light }}>
            <View style={[GsStyles.Flex.Row, GsStyles.Flex.JustifyContentSpaceBetween]}>
                <GsButton onPress={onClickBack}>
                    <GsIcon name={Icons.Back} color={Theme.colors.dark} size='medium'></GsIcon>
                    <Text style={GsStyles.Typography.H6}>Back</Text>
                </GsButton>
                <GsButton onPress={onClickDeleteItem}>
                    <GsIcon name={Icons.Delete} color={Theme.colors.danger} size='medium'></GsIcon>
                </GsButton>
            </View>

            <View style={[GsStyles.Flex.Row, GsStyles.Flex.AlignItemsCenter, GsStyles.Spacing.PaddingVertical, GsStyles.Colors.BgLight]}>
                <CheckBoxComponent value={state.Item.IsCompleted} onPress={() => { setState({ ...state, Status: ModifyStatus.MODIFIED, Item: { ...state.Item, IsCompleted: !state.Item.IsCompleted } }) }}></CheckBoxComponent>
                <TextInput style={state.Item.IsCompleted ? styles.itemTitleChecked : styles.itemTitle} value={state.Item.Title} onChangeText={(newText: string) => { setState({ ...state, Status: ModifyStatus.MODIFIED, Item: { ...state.Item, Title: newText } }) }}></TextInput>
                <BookMarkComponent value={state.Item?.IsBookMarked} onPress={() => { setState({ ...state, Status: ModifyStatus.MODIFIED, Item: { ...state.Item, IsBookMarked: !state.Item.IsBookMarked } }) }}></BookMarkComponent>
            </View>

            <ScrollView style={{ backgroundColor: Theme.colors.background }}>

                <View style={[GsStyles.Colors.BgLight, GsStyles.Borders.BorderRadiusSmall, GsStyles.Spacing.MarginTop, GsStyles.SpacingSmall.MarginHorizontal]}>
                    <GsButton style={GsStyles.Spacing.NoPaddingVertical} onPress={onClickImplementingFeature}>
                        <GsIcon name={Icons.Reminder} size="large"></GsIcon>
                        <View style={[GsStyles.Flex.Fill, GsStyles.Spacing.MarginLeft, GsStyles.Spacing.PaddingVertical, GsStyles.Borders.BorderBottom]}>
                            <Text >Remind Me</Text>
                        </View>
                    </GsButton>
                    <GsButton style={GsStyles.Spacing.NoPaddingVertical} onPress={onClickImplementingFeature}>
                        <GsIcon name={Icons.Date} size="large"></GsIcon>
                        <View style={[GsStyles.Flex.Fill, GsStyles.Spacing.MarginLeft, GsStyles.Spacing.PaddingVertical, GsStyles.Borders.BorderBottom]}>
                            <Text >Add Due Date</Text>
                        </View>
                    </GsButton>
                    <GsButton style={GsStyles.Spacing.NoPaddingVertical} onPress={onClickImplementingFeature}>
                        <GsIcon name={Icons.Repeat} size="large"></GsIcon>
                        <View style={[GsStyles.Flex.Fill, GsStyles.Spacing.MarginLeft, GsStyles.Spacing.PaddingVertical, GsStyles.Borders.BorderBottom]}>
                            <Text >Repeat</Text>
                        </View>
                    </GsButton>
                </View>

                <TodoImagesComponent images={state.Item.Images} onChange={(images) => { setState({ ...state, Status: ModifyStatus.MODIFIED, Item: { ...state.Item, Images: images } }) }}></TodoImagesComponent>

                <TouchableWithoutFeedback onPress={() => { setState({ ...state, IsEditingNote: true }) }}>
                    <View>
                        <Text style={[styles.note, { color: state.Item.Note ? Theme.colors.dark : Theme.colors.gray }]}>
                            {state.Item.Note ? state.Item.Note : 'Add note'}
                        </Text>
                    </View >
                </TouchableWithoutFeedback>

                <View style={[GsStyles.Spacing.Padding, GsStyles.Spacing.MarginTop]}>
                    <Button disabled={state.Status !== ModifyStatus.MODIFIED} onPress={() => { updateItem(state.Item); setState({ ...state, Status: ModifyStatus.SAVED }) }} title={state.Status === ModifyStatus.SAVED ? "Saved" : "Save changes"}></Button>
                </View>

            </ScrollView>
            {
                state.IsEditingNote && <Modal animationType='slide' presentationStyle='overFullScreen' transparent={true} visible={true}>
                    <InputFormModal title={state.Item.Title} placeholder="Add note" value={state.Item.Note} onDissmiss={(value) => {
                        if (value !== state.Item.Note) {
                            setState({
                                ...state,
                                Item: {
                                    ...state.Item,
                                    Note: value
                                },
                                Status: ModifyStatus.MODIFIED,
                                IsEditingNote: false
                            });
                        } else {
                            setState({
                                ...state,
                                IsEditingNote: false
                            });
                        }
                    }} />
                </Modal>
            }
        </SafeAreaView>
    </KeyboardAvoidingView >
}