import { StyleSheet } from "react-native";

const DetailPageStyles = StyleSheet.create({
    container: {
        position: 'relative',
        flex: 1,
        width: '100%'
    },
    listContainer: {
        backgroundColor: 'white',
        borderRadius: 4,
        marginLeft: 10,
        marginRight: 10,
        marginTop: 16,
    },
    itemContainer: {
        display: "flex",
        flexDirection: 'row',
        alignItems: "center"
    },
    todoItem:{
        display: "flex",
        flexDirection: 'row',
        alignItems: "center",
        paddingVertical: 16,
        backgroundColor:'white'
    },
    itemCheckBox :{

    },
    itemTitle:{
        fontSize: 20,
        flex:1,
    },
    itemTitleChecked:{
        fontSize: 18,
        flex:1,
        textDecorationLine: "line-through"
    },
    itemIcon: {
        marginLeft: 16,
        marginRight: 16,
        width: 20,
        height: 20
    },
    itemText: {
        paddingVertical: 16,
        marginLeft: 16,
        flex: 1
    },
    inlineItem:{
        borderBottomColor: "#F0F0F0",
        borderBottomWidth: 1,
        paddingVertical: 16,
        marginLeft: 16,
        flex: 1
    },
    note: {
        backgroundColor: 'white',
        minHeight: 80,
        margin: 10,
        padding: 16,
        paddingTop: 16,
        borderRadius: 4
    }
});
export default DetailPageStyles;