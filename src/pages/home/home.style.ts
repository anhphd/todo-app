import { StyleSheet } from "react-native";

const HomeStyles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        backgroundColor: '#F0F0F0'
    },
    
    inputContainer: {
        position: 'absolute',
        bottom: 0,
        width: '100%'
    },

    inputElement: {
        display: "flex",
        flexDirection: 'row',
        backgroundColor: 'white',
        alignItems: 'center',
        paddingHorizontal: 16
    },

    inputCheckBox: {
        paddingLeft: 16
    },

    inputText: {
        backgroundColor: 'white',
        borderRadius: 4,
        flex: 1,
        paddingTop: 16,
        paddingBottom: 16
    },
    inputSubmit: {
        paddingLeft: 16,
        paddingRight: 16
    },
    fabButton: {
        position: 'absolute',
        right: 16,
        bottom: 32,
    }
});
export default HomeStyles;