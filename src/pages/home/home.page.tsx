import React, { useState, useContext, useEffect, useRef } from 'react';
import { View, KeyboardAvoidingView, Text, TextInput, Modal, Platform, StatusBar, SafeAreaView, FlatList } from "react-native";
import HomeStyles from './home.style';
import { FabButton } from '../../components/fab-button.component';
import { CheckBoxComponent } from '../../components/checkbox.component';
import { GsIcon, Icons } from '../../components/gs-icon/gs-icon.component';
import { StringUtils } from '../../utils/string.util';
import { StackNavigationProp } from '@react-navigation/stack';
import { SettingModal } from '../../components/modal/setting-modal.component';
import { GsButton } from '../../components/gs-button.component';
import { BackDropModal } from '../../components/modal/backdrop-modal.component';
import { TodoListComponent } from '../../components/todo-list.component';
import { TodoListFilterComponent } from '../../components/todo-list-filter.component';
import { TodoContext } from '../../context/todo.context';
import { ITodoItem, TodoItem } from '../../models/todo.model';

import * as GsStyles from '../../styles/index';
import { CurrentTheme as Theme } from '../../config/theme.config';

const styles = HomeStyles;

type HomPageProps = { navigation: StackNavigationProp<any> };

const TodoInputForm = ({ onSubmit }: { onSubmit: (text: string, closeOnSubmit: boolean) => void }) => {

    const [text, setText] = useState('');

    return <View style={styles.inputContainer}>
        <View style={[GsStyles.Flex.Row, GsStyles.Flex.AlignItemsCenter, GsStyles.Spacing.PaddingHorizontal, GsStyles.Colors.BgLight]}>
            <CheckBoxComponent value={false}></CheckBoxComponent>
            <TextInput
                style={[GsStyles.Flex.Fill, GsStyles.Spacing.PaddingVertical, GsStyles.Borders.BorderRadiusSmall]}
                autoFocus={true}
                blurOnSubmit={true}
                placeholder="Add a tasks"
                onChangeText={value => setText(value)}
                value={text}
                onBlur={() => {
                    onSubmit('', true);
                    setText('');
                }}
                onSubmitEditing={() => {
                    onSubmit(text, true);
                    setText('');
                }} />

            <GsButton onPress={() => {
                onSubmit(text, false);
                setText('');
            }}>
                <GsIcon name={text ? Icons.SubmitEnabled : Icons.Submit} size="large"></GsIcon>
            </GsButton>
        </View>
    </View>
}

export const HomePage = ({ navigation }: HomPageProps) => {
    const { todos, addItem } = useContext(TodoContext);
    const [editing, setEditing] = useState(false);
    const [newAddedItemId, setNewAddedItemId] = useState('');
    const [showSetting, setShowSetting] = useState(false);

    let todoListRef = useRef<FlatList<ITodoItem>>(null);

    useEffect(() => {
        if (newAddedItemId) {
            let index = todos.findIndex(ele => { return ele.ItemId === newAddedItemId });
            setTimeout(() => { if (index != -1 && todoListRef) todoListRef.current?.scrollToIndex({ animated: true, index: index }); }, 400);
        }
    }, [newAddedItemId]);


    if (Platform.OS == 'android') {
        useEffect(() => {
            navigation.addListener('focus', () => {
                StatusBar.setBarStyle('dark-content');
                StatusBar.setBackgroundColor(Theme.colors.background);
            });

            return () => {
                navigation.removeListener('focus', () => { });
            }
        }, []);
    }


    return <KeyboardAvoidingView style={styles.container} behavior={Platform.OS == 'ios' ? 'padding' : undefined}>
        <SafeAreaView style={styles.container}>
            <View style={[GsStyles.Flex.Row, GsStyles.Spacing.Padding]}>
                <View style={GsStyles.Flex.Fill}>
                    <Text style={GsStyles.Typography.H4}>Tasks</Text>
                    <Text style={[GsStyles.Typography.Subtitle2, GsStyles.Colors.Gray, GsStyles.SpacingSmall.PaddingTop]}>{new Date().toDateString()}</Text>
                </View>
                <GsButton onPress={() => setShowSetting(true)}>
                    <GsIcon name={Icons.More} color={Theme.colors.dark}></GsIcon>
                </GsButton>
            </View>

            <View style={[GsStyles.Flex.Fill]}>
                <TodoListFilterComponent></TodoListFilterComponent>
                <TodoListComponent ref={todoListRef}></TodoListComponent>
                <View style={{ height: 66 }}></View>
            </View>

            <FabButton onPress={() => setEditing(true)} visible={!editing}></FabButton>

            {
                editing && <TodoInputForm onSubmit={(text, closeOnSubmit) => {
                    if (closeOnSubmit) setEditing(false);
                    if (!StringUtils.IsNullOrEmpty(text)) {
                        let newItem: ITodoItem = new TodoItem();
                        newItem.ItemId = '' + Date.now();
                        newItem.Title = text;
                        addItem(newItem);
                        setNewAddedItemId(newItem.ItemId);
                    }
                }} />
            }

            <Modal animationType='slide' presentationStyle='overFullScreen' transparent={true} visible={showSetting}>
                <SettingModal dismiss={() => {
                    setShowSetting(false);
                }}></SettingModal>
            </Modal>
        </SafeAreaView>


    </KeyboardAvoidingView>
}
