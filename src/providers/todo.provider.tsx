import React, { useReducer, useEffect } from "react";
import { TodoContext, TaskSettings, defaultTodoState, ITodoContext, ITodoState } from "../context/todo.context";

import { ITodoItem } from "../models/todo.model";
import { StorageService } from "../services/storage.service";
import { TodoReducer, TodoActionType } from "../reducers/todo.reducer";

export const TodoProvider = ({ children }: { children: any }) => {

    const [todoState, todoStateDispatch] = useReducer(TodoReducer, defaultTodoState);

    const todoContext: ITodoContext = {

        ...todoState,

        setItems: (items: Array<ITodoItem>) => {
            todoStateDispatch({
                type: TodoActionType.SET_ITEMS,
                payload: {
                    Items: items
                }
            });
        },

        getItem: (itemId: string): ITodoItem => {
            let item = todoState.todos.find(ele => ele.ItemId === itemId);
            if (item) return item;
            throw new Error(`${itemId} not exist`);
        },

        removeItem: (itemId: string) => {
            todoStateDispatch({
                type: TodoActionType.REMOVE_ITEM,
                payload: {
                    ItemId: itemId
                }
            });
        },

        removeAllItems: () => {
            todoStateDispatch({
                type: TodoActionType.REMOVE_ALL_ITEMS,
                payload: {}
            });
        },

        updateItem: (item: ITodoItem) => {
            todoStateDispatch({
                type: TodoActionType.UPDATE_ITEM,
                payload: {
                    Item: item
                }
            });
        },

        addItem: (item: ITodoItem) => {
            todoStateDispatch({
                type: TodoActionType.ADD_ITEM,
                payload: {
                    Item: item
                }
            });
        },

        toggleCompleted: (itemId: string) => {
            todoStateDispatch({
                type: TodoActionType.TOGGLE_COMPLETED,
                payload: {
                    ItemId: itemId
                }
            });
        },

        updateSettings: (settings: TaskSettings) => {
            todoStateDispatch({
                type: TodoActionType.UPDATE_SETTING,
                payload: {
                    settings: settings
                }
            });
        }
    }

    useEffect(() => {
        StorageService.fetchItem<ITodoState>("_TODO_STATE_").then(data => {
            if (data) {
                todoContext.setItems(data.todos);
                todoContext.updateSettings(data.settings);
            }
        })
    }, []);

    useEffect(() => {
        StorageService.saveItem(todoState, "_TODO_STATE_");
    }, [todoState]);

    return <TodoContext.Provider value={todoContext}>{children}</TodoContext.Provider>
}
