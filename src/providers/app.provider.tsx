import React, { useReducer } from "react";
import { AppContext, defaultAppState } from "../context/app.context";
import { AppStateReducer, AppActionType } from "../reducers/app.reducer";

export const AppProvider = ({ children }: { children: any }) => {

    const [appState, appStateDispatch] = useReducer(AppStateReducer, defaultAppState);

    const appContext = {
        ...appState,
        setLoading: (loading: boolean = true) => {
            appStateDispatch({
                type: AppActionType.SetLoading, payload: {
                    isLoading: loading
                }
            });
        },
        useTheme: (themeName: string) => {
            appStateDispatch({
                type: AppActionType.UseTheme, payload: {
                    theme: themeName
                }
            });
        }
    }

    return <AppContext.Provider value={appContext}>{children}</AppContext.Provider>
}
