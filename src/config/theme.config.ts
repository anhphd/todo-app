import { ITheme } from "../models/theme.model";

export const ThemeConfig = {
    default: {
        colors: {
            primary: "#FF6721",
            secondary: "#2196F3",
            danger: "#E80505",
            warning: "#FFCB00",
            gray: "#808080",
            light: "#FFFFFF",
            dark: "#000000",
            background: "#F0F0F0"
        },
        fontSizes: {
            base: 16,
            h1: 96,
            h2: 60,
            h3: 48,
            h4: 34,
            h5: 24,
            h6: 20,
            subtitle1: 16,
            subtitle2: 14,
            body1: 16,
            body2: 14,
            button: 14,
            caption: 12,
            overline: 10,
        },
        buttonSizes: {
            small: 12,
            medium: 16,
            large: 20,
            xlarge: 24,
            xxlarge: 32,
            xxxlarge: 48
        }
    }
}

export var CurrentTheme: ITheme = ThemeConfig.default;

/**
 * Load a theme base on given name, if theme name not exist, it will fallback to default.
 * @param name theme name
 */
export const useTheme = (name: string) => {
    let theme = ThemeConfig.default
    if (name in Object.keys(ThemeConfig)) {
        theme = (<any>ThemeConfig)[name];
    }
    CurrentTheme = {
        ...theme
    }
}