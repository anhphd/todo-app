import React from "react";
import { ITodoItem } from "../models/todo.model";

export enum SortBy {
    NONE = 'None',
    MODIFIED_AT = 'Modified Time',
    CREATED_AT = 'Creation Date',
    NAME = 'Name',
    PRIORITY = 'Priority'
}

export enum SortType {
    INCREASE,
    DECREASE
}

export interface TaskSettings {
    sortBy: SortBy,
    sortType: SortType,
    hideCompletedTask: boolean
}

export interface ITodoState {
    todos: Array<ITodoItem>,
    settings: TaskSettings,
}
export const defaultTodoState: ITodoState = {
    todos: [],
    settings: {
        sortBy: SortBy.NONE,
        sortType: SortType.DECREASE,
        hideCompletedTask: false
    }
}

export interface ITodoContext extends ITodoState {
    setItems: (items: Array<ITodoItem>) => void;
    getItem: (itemId: string) => ITodoItem;
    removeItem: (itemId: string) => void;
    removeAllItems: () => void;
    updateItem: (item: ITodoItem) => void;
    addItem: (item: ITodoItem) => void;
    toggleCompleted: (itemID: string) => void;
    updateSettings: (settings: TaskSettings) => void;
}

export const defaultTodoContext: ITodoContext = {
    todos: [],
    settings: {
        sortBy: SortBy.NONE,
        sortType: SortType.INCREASE,
        hideCompletedTask: false
    },
    setItems: (items: Array<ITodoItem>) => { },
    getItem: (itemId: string): ITodoItem => { throw Error(); },
    removeItem: (itemId: string) => { },
    removeAllItems: () => { },
    updateItem: (item: ITodoItem) => { },
    addItem: (item: ITodoItem) => { },
    toggleCompleted: (itemID: string) => { },
    updateSettings: (settings: TaskSettings) => { }
}


export const TodoContext = React.createContext<ITodoContext>(defaultTodoContext);