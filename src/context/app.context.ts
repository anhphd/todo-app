import React from "react";

export interface IAppState {
    isLoading: boolean,
    theme: string
}

export interface IAppContext extends IAppState {
    setLoading: (loading: boolean) => void;
    useTheme: (name: string) => void;
}

export const defaultAppState: IAppState = {
    isLoading: true,
    theme: 'default'
}

export const defaultAppContext = {
    ...defaultAppState,
    setLoading: (loading: boolean = true) => { },
    useTheme: (name: string) => { }
}

export const AppContext = React.createContext<IAppContext>(defaultAppContext);

