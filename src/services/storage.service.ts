import AsyncStorage from '@react-native-community/async-storage';

export class StorageService {

    private static _KEY_ = "TASKS";

    static async fetchItems<T>(key?: string): Promise<Array<T> | null> {
        try {
            let value = await AsyncStorage.getItem(key ? key : this._KEY_);
            if (value != null) {
                let tasks: Array<T> = JSON.parse(value);
                return tasks;
            }
        } catch (err) {
            console.log("error", err);
            return null;
        }
        return null;
    }


    static async saveItems<T>(items: Array<T>, key?: string): Promise<void> {
        try {
            await AsyncStorage.setItem(key ? key : this._KEY_, JSON.stringify(items));
        } catch (err) {
            console.log("error", err);
        }
    }

    static async fetchItem<T>(key: string): Promise<T | null> {
        try {
            let value = await AsyncStorage.getItem(key);
            if (value != null) {
                let t: T = JSON.parse(value);
                return t;
            }
        } catch (err) {
            console.log("error", err);
            return null;
        }
        return null;
    }


    static async saveItem<T>(item: T, key: string): Promise<void> {
        try {
            await AsyncStorage.setItem(key, JSON.stringify(item));
        } catch (err) {
            console.log("error", err);
        }
    }

}