export interface ITodoItem {
    ItemId: string;
    Title: string;
    Description: string;
    CreatedAt: number;
    ModifiedAt: number;
    CompletedAt?: number;
    DueDateAt?: number;
    CreatedBy: string;
    IsCompleted: boolean;
    IsBookMarked: boolean;
    Note: string;
    Type: string;
    Priority: number;
    Images: Array<string>;
}

export class TodoItem implements ITodoItem {
    ItemId: string = "";
    Title: string = "";
    Description: string = "";
    CreatedAt: number = Date.now();
    ModifiedAt: number = Date.now();
    CreatedBy: string = "";
    IsCompleted: boolean = false;
    IsBookMarked: boolean = false;
    Note: string = "";
    Type: string = "";
    Priority: number = 0;
    Images: Array<string> = []
}