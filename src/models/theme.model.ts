
export interface ITheme {
    colors: {
        primary: string,
        secondary: string,
        danger: string,
        warning: string,
        gray: string,
        light: string,
        dark: string,
        background: string,
    },
    fontSizes: {
        base: number,
        h1: number,
        h2: number,
        h3: number,
        h4: number,
        h5: number,
        h6: number,
        subtitle1: number,
        subtitle2: number,
        body1: number,
        body2: number,
        button: number,
        caption: number,
        overline: number
    },
    buttonSizes: {
        small: number,
        medium: number,
        large: number,
        xlarge: number,
        xxlarge: number,
        xxxlarge: number
    },
}
