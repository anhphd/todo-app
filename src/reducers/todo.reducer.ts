import { ITodoState, ITodoContext, TaskSettings, SortBy, SortType } from "../context/todo.context";
import { ITodoItem } from "../models/todo.model";

export enum TodoActionType {
    TOGGLE_COMPLETED,
    TOGGLE_BOOKMARK,
    ADD_ITEM,
    SET_ITEMS,
    REMOVE_ITEM,
    REMOVE_ALL_ITEMS,
    UPDATE_ITEM,
    UPDATE_SETTING
};


type SetItemsActionPayload = {
    Items: Array<ITodoItem>
}
type AddItemActionPayload = {
    Item: ITodoItem
}
type UpdateItemPayload = {
    Item: ITodoItem
}
type RemoveItemPayload = {
    ItemId: string
}
type ToggleCompletedPayload = {
    ItemId: string
}
type ToggleBookMarkPayload = {
    ItemId: string
}

type UpdateSettingsPayload = {
    settings: TaskSettings
}

type RemoveItemsPayload = {
  
}

export type ITodoActionPayload = AddItemActionPayload | SetItemsActionPayload | RemoveItemPayload | UpdateItemPayload | UpdateSettingsPayload | ToggleBookMarkPayload | ToggleCompletedPayload | RemoveItemsPayload;


const sortTasks = (tasks: Array<ITodoItem>, settings: TaskSettings) => {
    let items = [...tasks];
    switch (settings.sortBy) {
        case SortBy.MODIFIED_AT:
            items = items.sort((a, b) => {
                return settings.sortType == SortType.INCREASE ? (b.ModifiedAt - a.ModifiedAt) : (a.ModifiedAt - b.ModifiedAt);
            })
            break;
        case SortBy.CREATED_AT:
            items = items.sort((a, b) => {
                return settings.sortType == SortType.INCREASE ? (b.CreatedAt - a.CreatedAt) : (a.CreatedAt - b.CreatedAt);
            })
            break;
        case SortBy.NAME:
            items = items.sort((a, b) => {
                if (b.Priority == a.Priority) {
                    return settings.sortType == SortType.INCREASE ? b.Title.localeCompare(a.Title) : a.Title.localeCompare(b.Title); return a.Title.localeCompare(b.Title);
                }
                return b.Priority - a.Priority;
            })
            break;
        case SortBy.PRIORITY:
            items = items.sort((a, b) => {
                return settings.sortType == SortType.INCREASE ? (b.Priority - a.Priority) : (a.Priority - b.Priority);
            })
            break;

        default:

    }

    return items;
}

export interface ITodoAction {
    type: TodoActionType,
    payload: ITodoActionPayload
}

export const TodoReducer = (preState: ITodoState, action: ITodoAction): ITodoState => {
    switch (action.type) {
        case TodoActionType.ADD_ITEM:
            return {
                ...preState, todos: [...sortTasks([...preState.todos, (action.payload as AddItemActionPayload).Item], preState.settings)]
            }
        case TodoActionType.SET_ITEMS:
            const { Items } = action.payload as SetItemsActionPayload;
            return {
                ...preState, todos: [...Items]
            }
        case TodoActionType.REMOVE_ITEM:
            const { ItemId } = action.payload as RemoveItemPayload;
            return {
                ...preState,
                todos: preState.todos.filter(item => {
                    return item.ItemId !== ItemId
                })
            }
        case TodoActionType.REMOVE_ALL_ITEMS:
            return {
                ...preState,
                todos: []
            }
        case TodoActionType.UPDATE_ITEM:
            let { Item } = action.payload as UpdateItemPayload;
            return {
                ...preState, todos: [...sortTasks(preState.todos.map(item => {
                    if (item.ItemId === Item.ItemId) {
                        Item.ModifiedAt = Date.now();
                        return Item;
                    }
                    return item;
                }), preState.settings)]
            }
        case TodoActionType.TOGGLE_COMPLETED:
            const toggleCompletedPayload = action.payload as ToggleCompletedPayload;
            return {
                ...preState, todos: [...sortTasks(preState.todos.map(item => {
                    if (item.ItemId === toggleCompletedPayload.ItemId) {
                        return {
                            ...item,
                            IsCompleted: !item.IsCompleted
                        }
                    }
                    return item;
                }), preState.settings)]
            }
        case TodoActionType.TOGGLE_BOOKMARK:
            const toggleBookmarkedPayload = action.payload as ToggleBookMarkPayload;
            return {
                ...preState, todos: [...sortTasks(preState.todos.map(item => {
                    if (item.ItemId === toggleBookmarkedPayload.ItemId) {
                        return {
                            ...item,
                            IsBookMarked: !item.IsBookMarked
                        }
                    }
                    return item;
                }), preState.settings)]
            }
        case TodoActionType.UPDATE_SETTING:
            const { settings } = action.payload as UpdateSettingsPayload;
            return {
                ...preState,
                todos: [...sortTasks(preState.todos, settings)], settings: settings
            }

        default:
            return preState;

    }
};