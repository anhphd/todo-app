import { IAppState } from "../context/app.context";
import { useTheme } from "../config/theme.config";

export enum AppActionType {
    SetLoading, UseTheme
};

type LoadingActionPayload = {
    isLoading: boolean
}

type UseThemeActionPayload = {
    theme: string
}

export type AppActionPayload = LoadingActionPayload | UseThemeActionPayload;

export interface IAppAction {
    type: AppActionType,
    payload?: AppActionPayload
}

export const AppStateReducer = (preState: IAppState, action: IAppAction): IAppState => {
    switch (action.type) {
        case AppActionType.SetLoading:
            return { ...preState, isLoading: (action.payload as LoadingActionPayload).isLoading };

        case AppActionType.UseTheme:
            const { theme } = action.payload as UseThemeActionPayload;
            useTheme(theme);
            return { ...preState, theme: theme };

        default:
            return preState;
    }
};